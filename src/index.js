const express = require("express");

const app = express();

app.get("/", async (req, res) => {
  try {
    res.send("Testing the server");
  } catch (error) {
    throw new Error(error.message);
  }
});

const server = app.listen(5000, () =>
  console.log(`Server started on port => 5000!`)
);

process.on("SIGTERM", () => {
  server.close(() => console.log(`Process terminated!`));
});
